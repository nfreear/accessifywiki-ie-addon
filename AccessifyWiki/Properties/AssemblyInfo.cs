﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AccessifyWiki")]  //"File description"
[assembly: AssemblyDescription("AccessifyWiki add-on for Internet Explorer | http://accessify.wikia.com")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("AccessifyWiki add-on for Internet Explorer | http://accessify.wikia.com")]  //"Product name"
[assembly: AssemblyCopyright("Copyright © 2014 Nick Freear and contributors.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
//[assembly: Guid("dcbf91ca-edf7-46d1-a022-2e6f0e6cd597")]
// http://www.guidgenerator.com/
[assembly: Guid("674daf9b-854e-4277-91f9-2e2f125425b4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
