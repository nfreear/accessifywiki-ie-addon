﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHDocVw;
using mshtml;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Windows.Forms;

namespace AccessifyWiki.BHO
{

    [
        ComVisible(true),
        //Guid("8a194578-81ea-4850-9911-13ba2d71efbd"),
        // http://www.guidgenerator.com/
        Guid("0a8c774f-3f70-45e5-a9be-5e9ac66d6176"),
        ClassInterface(ClassInterfaceType.None)
    ]

    public class BHO:IObjectWithSite
    {
        SHDocVw.WebBrowser webBrowser;

        public static string BHOKEYNAME =
          "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";

        public static string BHOKEYNAME_WOW64 =
          "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";

        public static string JAVASCRIPT_URL =
          "//accessifywiki.appspot.com/browser/js/accessifyhtml5-marklet.js";


        [ComRegisterFunction]
        public static void RegisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);

            if (registryKey == null)
                registryKey = Registry.LocalMachine.CreateSubKey(BHOKEYNAME);

            string guid = type.GUID.ToString("B");
            RegistryKey ourKey = registryKey.OpenSubKey(guid);

            if (ourKey == null)
                ourKey = registryKey.CreateSubKey(guid);

            ourKey.SetValue("Alright", 1);
            ourKey.SetValue("NoExplorer", 1); //REG_DWORD
            registryKey.Close();
            ourKey.Close();
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);
            string guid = type.GUID.ToString("B");

            if (registryKey != null)
                registryKey.DeleteSubKey(guid, false);
        }

        public void OnDocumentComplete(object pDisp, ref object URL)
        {
            IHTMLDocument2 doc = (IHTMLDocument2)webBrowser.Document;
            doc.parentWindow.execScript(
              "window.AC5U={app_id:'msie-addon'};" +
              "var d=window.document,s=d.createElement('script')," +
              "h=d.getElementsByTagName('body')[0];" +
              "s.src='" + JAVASCRIPT_URL + "';h.appendChild(s);"
            );
        }
 
        public int SetSite(object site)
        {
            if (site != null)
            {
                webBrowser = (SHDocVw.WebBrowser)site;
                webBrowser.DocumentComplete +=
                  new DWebBrowserEvents2_DocumentCompleteEventHandler(
                  this.OnDocumentComplete);
            }
            else
            {
                webBrowser.DocumentComplete -=
                  new DWebBrowserEvents2_DocumentCompleteEventHandler(
                  this.OnDocumentComplete);
                webBrowser = null;
            }

            return 0;
        }

        public int GetSite(ref Guid guid, out IntPtr ppvSite)
        {
            IntPtr punk = Marshal.GetIUnknownForObject(webBrowser);
            int hr = Marshal.QueryInterface(punk, ref guid, out ppvSite);
            Marshal.Release(punk);
            return hr;
        }

    }
}
