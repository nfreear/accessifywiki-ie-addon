﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AccessifyWiki.BHO
{
[
    ComVisible(true),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
    //Guid("FC4801A3-2BA9-11CF-A229-00AA003D7352")
    // http://www.guidgenerator.com/
    Guid("d6fadfaf-5b52-4f47-bda0-dc5f4870f346")
]
    public interface IObjectWithSite
    {
        [PreserveSig]
        int SetSite([MarshalAs(UnmanagedType.IUnknown)]object site);
        [PreserveSig]
        int GetSite(ref Guid guid, out IntPtr ppvSite);
    }
}
